# pi

These are simple and **opinionated** instructions on how to install a simple suite of apps to manage your movie and tv show collections in a raspberry pi. The same instructions can be easily be adapted to run on a normal computer.

The services are systemd unit files that create docker containers, with mostly images created by the linuxserver.io (https://www.linuxserver.io/).
If you prefer to run the docker image manually you just need to copy and paste the `ExecStart` line.

These are not in-depth instructions on how to configure the services. To find more about each service and how to configure them please refer to the corresponding linuxserver.io docker image documentation (https://fleet.linuxserver.io/) or to the service website itself, as below:

* Radarr: Manages your movie library - https://radarr.video/
* Sonarr: Manages your TV library - https://sonarr.tv/
* Jackett: Meta indexer for torrent sites - https://github.com/Jackett/Jackett
* Bazarr: Manages subtitles - https://www.bazarr.media/
* Transmission: torrent client - https://transmissionbt.com/
* Plex: Streaming and media service and a client–server media player - https://www.plex.tv/
* Pi-hole: DNS Ad Blocker - https://pi-hole.net/

## Install Instructions

1. Enable the 64-bit kernel on your raspberry pi (https://forums.raspberrypi.com/viewtopic.php?f=29&t=250730)
2. You will need a path (preferably an external hard drive if you are running on a pi). The examples use the directory `tr/` inside the mountpoint `/mnt/media1`. If your path is different, please update accordingly.
3. Copy each `.service` file to `/etc/systemd/system/` directory.
4. (Optional) On `plex.service`: You will need to register your plex using `PLEX_CLAIM` if you want your server to be automatically logged in. It is optional, you can just remove the whole line if you don't want it.
5. (Optional) On `pihole.service` you can replace `myhost.example.com` with your domain if you want.
6. As these images are intended for the 64-bit kernel, the docker for armhf has an issue running it with the actual seccomp profile. A workaround is to replace the `architectures` entry in the default seccomp with `[ "SCMP_ARCH_AARCH64", "SCMP_ARCH_ARM" ]`. The `etc/docker/seccomp_default.json` provided in this repo already have this change, but if you feel unsure to load a random profile from the internet (as I would be), you can just replace it on your file. Ref: https://github.com/moby/moby/issues/41092
7. Register the new service unit files: `sudo systemctl daemon-reload`
8. (Optional) Set the services to automatically startup: `sudo systemctl enable bazarr jackett pihole plex radarr sonarr transmission`
9. Start the services: `sudo systemctl start bazarr jackett pihole plex radarr sonarr transmission`
10. Profit!

Each service is running on its own port:  
bazarr: http://YOUR_SERVER:9003  
jackett: http://YOUR_SERVER:9004  
pihole: http://YOUR_SERVER:80  
radarr: http://YOUR_SERVER:9001  
sonarr: http://YOUR_SERVER:9002  
transmission: http://YOUR_SERVER:9000  

If you want to change the ports, just update the `-p` lines on the unit file.

## Configuration Instructions

The jackett is the first service you will need to configure. It will index your torrent sites and make them available to radarr and sonarr.  
On transmission I recommend that you enable the rpc for localhost and your internal network.  
After jackett and transmission are configured, you should be able to connect both radarr and sonarr to them. 
